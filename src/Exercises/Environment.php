<?php

declare(strict_types=1);

namespace UMA\Raytracer\Exercises;

use UMA\Raytracer\Core\Tuple;

final class Environment
{
    private $gravity;

    private $wind;

    public function __construct(Tuple $gravity, Tuple $wind)
    {
        $this->gravity = $gravity;
        $this->wind = $wind;
    }

    public function gravity(): Tuple
    {
        return $this->gravity;
    }

    public function wind(): Tuple
    {
        return $this->wind;
    }
}
