<?php

declare(strict_types=1);

namespace UMA\Raytracer\Exercises;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UMA\Raytracer\Core\Canvas;
use UMA\Raytracer\Core\Tuple;

final class Capstone2 extends Command
{
    protected function configure()
    {
        $this
            ->setName('c:2')
            ->setDescription('Outputs a PPM ASCII file with a print of the projectile simulator from Capstone 1')
            ->addArgument('width', InputArgument::OPTIONAL, 'Canvas Width', 900)
            ->addArgument('height', InputArgument::OPTIONAL, 'Canvas Height', 550);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $width = (int) $input->getArgument('width');
        $height = (int) $input->getArgument('height');

        $start = Tuple::point(0.0, 1.0, 0.0);
        $velocity = Tuple::mul(11.25, Tuple::vector(1.0, 1.8, 0.0)->normalize());
        $projectile = new Projectile($start, $velocity);

        $gravity = Tuple::vector(0.0, -0.1, 0.0);
        $wind = Tuple::vector(-0.01, 0.0, 0.0);
        $environment = new Environment($gravity, $wind);

        $canvas = new Canvas($width, $height);

        $i = 0;
        while ($projectile->position()->y() >= 0) {
            $x = (int)$projectile->position()->x();
            $y = $height - (int)$projectile->position()->y();

            if ($x < 0 || $x > $width || $y < 0 || $y > $height) {
                break;
            }

            $canvas->setPixel($x, $y, Tuple::color(1.0, 0.0, 0.0));

            $projectile = self::tick($environment, $projectile);

            $i++;
        }

        $output->writeln($canvas->toPPM());
        $output->writeln("# $width x $height pixels");
        $output->writeln(\sprintf('# %s peak memory (bytes)', number_format(memory_get_peak_usage(true))));

        return 0;
    }

    private static function tick(Environment $e, Projectile $p): Projectile
    {
        return new Projectile(
            Tuple::add($p->position(), $p->velocity()),
            Tuple::add(Tuple::add($p->velocity(), $e->gravity()), $e->wind())
        );
    }
}
