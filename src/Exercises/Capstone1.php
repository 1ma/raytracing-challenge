<?php

declare(strict_types=1);

namespace UMA\Raytracer\Exercises;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UMA\Raytracer\Core\Tuple;

final class Capstone1 extends Command
{
    protected function configure()
    {
        $this
            ->setName('c:1')
            ->setDescription('Runs a simple simulation of a projectile on a 3D environment with wind and gravity')
            ->addArgument('px', InputArgument::REQUIRED)
            ->addArgument('py', InputArgument::REQUIRED)
            ->addArgument('pz', InputArgument::REQUIRED)
            ->addArgument('vx', InputArgument::REQUIRED)
            ->addArgument('vy', InputArgument::REQUIRED)
            ->addArgument('vz', InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $initialPoint = Tuple::point((float) $input->getArgument('px'), (float) $input->getArgument('py'), (float) $input->getArgument('pz'));
        $initialVelocity = Tuple::vector((float) $input->getArgument('vx'), (float) $input->getArgument('vy'), (float) $input->getArgument('vz'));
        $projectile = new Projectile($initialPoint, $initialVelocity->normalize());

        // Gravity pulls a bit downward (-y) and wind slightly backwards (-x)
        $gravity = Tuple::vector(0.0, -0.1, 0.0);
        $wind = Tuple::vector(-0.01, 0.0, 0.0);
        $environment = new Environment($gravity, $wind);

        $i = 0;
        while ($projectile->position()->y() >= 0) {
            $projectile = self::tick($environment, $projectile);

            $i++;
        }

        $output->writeln("Simulation ran $i iterations");
        $output->writeln(\sprintf(
            'Final projectile position: (%.3f, %.3f, %.3f)',
            $projectile->position()->x(), $projectile->position()->y(), $projectile->position()->z()
        ));

        return 0;
    }

    private static function tick(Environment $e, Projectile $p): Projectile
    {
        return new Projectile(
            Tuple::add($p->position(), $p->velocity()),
            Tuple::add(Tuple::add($p->velocity(), $e->gravity()), $e->wind())
        );
    }
}
