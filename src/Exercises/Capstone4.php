<?php

declare(strict_types=1);

namespace UMA\Raytracer\Exercises;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UMA\Raytracer\Core\Canvas;
use UMA\Raytracer\Core\Material;
use UMA\Raytracer\Core\PointLight;
use UMA\Raytracer\Core\Ray;
use UMA\Raytracer\Core\Sphere;
use UMA\Raytracer\Core\Tuple;

final class Capstone4 extends Command
{
    protected function configure()
    {
        $this
            ->setName('c:4')
            ->setDescription('Outputs a PPM ASCII file of the silhouette of a sphere')
            ->addArgument('pixels', InputArgument::OPTIONAL, 'How many pixels per side', 128);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $rayOrigin = Tuple::point(0.0, 0.0, -5.0);
        $worldZ = 10.0;
        $wallSize = 8.0;
        $half = $wallSize / 2.0;

        $canvasPixels = (int) $input->getArgument('pixels');
        $pixelSize = $wallSize / $canvasPixels;
        $canvas = new Canvas($canvasPixels, $canvasPixels);

        $sphere = new Sphere();
        $sphere->setMaterial(new Material(Tuple::color(1.0, 0.0, 0.0), 0.1, 0.9, 0.9, 200.0));

        $light = new PointLight(Tuple::color(1.0, 1.0, 1.0), Tuple::point(-10.0, 10.0, -10.0));

        for ($y = 0; $y < $canvasPixels; $y++) {
            $worldY = $half - $pixelSize * $y;

            for ($x = 0; $x < $canvasPixels; $x++) {
                $worldX = -$half + $pixelSize * $x;
                $rayDirection = Tuple::sub(Tuple::point($worldX, $worldY, $worldZ), $rayOrigin);
                $ray = new Ray($rayOrigin, $rayDirection->normalize());

                if ($hit = $sphere->intersect($ray)->hit()) {
                    $point = $ray->position($hit->t());
                    $normal = $hit->shape()->normalAt($point);
                    $eye = Tuple::mul(-1.0, $ray->direction());

                    $canvas->setPixel($x, $y, $hit->shape()->getMaterial()->lightning($light, $point, $eye, $normal));
                }
            }
        }

        $output->writeln($canvas->toPPM());
        $output->writeln("# $canvasPixels x $canvasPixels pixels");
        $output->writeln(\sprintf('# %s peak memory (bytes)', number_format(memory_get_peak_usage(true))));

        return 0;
    }
}
