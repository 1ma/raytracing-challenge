<?php

declare(strict_types=1);

namespace UMA\Raytracer\Exercises;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UMA\Raytracer\Core\Canvas;
use UMA\Raytracer\Core\Matrix;
use UMA\Raytracer\Core\Tuple;

final class Capstone3 extends Command
{
    protected function configure()
    {
        $this
            ->setName('c:3')
            ->setDescription('Outputs a PPM ASCII file of a clock-like picture built with transformations');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $width = 100;
        $height = 100;

        $canvas = new Canvas($width, $height);

        $midnight = Tuple::point(0.0, 1.0, 0.0);

        $white = Tuple::color(1.0, 1.0, 1.0);

        $translation = Matrix::translation(50.0, 50.0, 0.0);
        $scaling = Matrix::scaling($width * 3/8, $height * 3/8, 0.0);

        for ($i = 0; $i < 12; $i++) {
            $transformation = Matrix::identity()
                ->apply(Matrix::rotationZ($i * M_PI / 6))
                ->apply($scaling)
                ->apply($translation);

            $hour = Matrix::mulTuple($transformation, $midnight);

            $canvas->setPixel((int) round($hour->x()), (int) round($hour->y()), $white);
        }

        $output->writeln($canvas->toPPM());
        $output->writeln("# $width x $height pixels");
        $output->writeln(\sprintf('# %s peak memory (bytes)', number_format(memory_get_peak_usage(true))));

        return 0;
    }
}
