<?php

declare(strict_types=1);

namespace UMA\Raytracer\Exercises;

use UMA\Raytracer\Core\Tuple;

final class Projectile
{
    private $position;

    private $velocity;

    public function __construct(Tuple $position, Tuple $velocity)
    {
        $this->position = $position;
        $this->velocity = $velocity;
    }

    public function position(): Tuple
    {
        return $this->position;
    }

    public function velocity(): Tuple
    {
        return $this->velocity;
    }
}
