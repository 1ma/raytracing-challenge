<?php

declare(strict_types=1);

namespace UMA\Raytracer\Core;

final class Ray
{
    /** @var Tuple */
    private $origin;

    /** @var Tuple */
    private $direction;

    public function __construct(Tuple $origin, Tuple $direction)
    {
        $this->origin = $origin;
        $this->direction = $direction;
    }

    public function origin(): Tuple
    {
        return $this->origin;
    }

    public function direction(): Tuple
    {
        return $this->direction;
    }

    public function position(float $t): Tuple
    {
        return Tuple::add($this->origin, Tuple::mul($t, $this->direction));
    }

    public function transform(Matrix $transformation): Ray
    {
        return new self(
            Matrix::mulTuple($transformation, $this->origin),
            Matrix::mulTuple($transformation, $this->direction)
        );
    }
}
