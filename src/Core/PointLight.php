<?php

declare(strict_types=1);

namespace UMA\Raytracer\Core;

final class PointLight
{
    /** @var Tuple */
    private $intensity;

    /** @var Tuple */
    private $position;

    public function __construct(Tuple $intensity, Tuple $position)
    {
        $this->intensity = $intensity;
        $this->position = $position;
    }

    public function intensity(): Tuple
    {
        return $this->intensity;
    }

    public function position(): Tuple
    {
        return $this->position;
    }
}
