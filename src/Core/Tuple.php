<?php

declare(strict_types=1);

namespace UMA\Raytracer\Core;

use function sqrt;

/**
 * The foundational data structure of the raytracer.
 *
 * It has three dimensions (x, y, z) and a fourth parameter (w) indicating
 * if it is a point (1.0) or a vector (0.0). These aren't semantically the
 * same, but otherwise are formed with a discrete value of the same three
 * dimensions.
 */
final class Tuple
{
    private $x;

    private $y;

    private $z;

    private $w;

    public function __construct(float $x, float $y, float $z, float $w)
    {
        $this->x = $x;
        $this->y = $y;
        $this->z = $z;
        $this->w = $w;
    }

    public static function point(float $x, float $y, float $z): Tuple
    {
        return new self($x, $y, $z, 1.0);
    }

    public static function vector(float $x, float $y, float $z): Tuple
    {
        return new self($x, $y, $z, 0.0);
    }

    public static function color(float $red, float $green, float $blue): Tuple
    {
        return self::vector($red, $green, $blue);
    }

    public static function equals(Tuple $a, Tuple $b): bool
    {
        return Floats::equivalent($a->x, $b->x)
            && Floats::equivalent($a->y, $b->y)
            && Floats::equivalent($a->z, $b->z)
            && Floats::equivalent($a->w, $b->w);
    }

    /**
     * Note: adding two points is illegal (w=2.0)
     */
    public static function add(Tuple $a, Tuple $b): Tuple
    {
        return new self(
            $a->x + $b->x,
            $a->y + $b->y,
            $a->z + $b->z,
            $a->w + $b->w
        );
    }

    /**
     * Note: subtracting a point from a vector is illegal (w=-1.0)
     */
    public static function sub(Tuple $a, Tuple $b): Tuple
    {
        return new self(
            $a->x - $b->x,
            $a->y - $b->y,
            $a->z - $b->z,
            $a->w - $b->w
        );
    }

    public static function dot(Tuple $a, Tuple $b): float
    {
        return $a->x * $b->x
            + $a->y * $b->y
            + $a->z * $b->z
            + $a->w * $b->w;
    }

    public static function cross(Tuple $a, Tuple $b): Tuple
    {
        return self::vector(
            $a->y * $b->z - $a->z * $b->y,
            $a->z * $b->x - $a->x * $b->z,
            $a->x * $b->y - $a->y * $b->x
        );
    }

    public static function neg(Tuple $t): Tuple
    {
        return self::sub(new self(0.0, 0.0, 0.0, 0.0), $t);
    }

    public static function mul(float $f, Tuple $t): Tuple
    {
        return new self(
            $f * $t->x,
            $f * $t->y,
            $f * $t->z,
            $f * $t->w
        );
    }

    public static function hadamard(Tuple $a, Tuple $b): Tuple
    {
        return self::color(
            $a->x * $b->x,
            $a->y * $b->y,
            $a->z * $b->z
        );
    }

    public static function div(float $f, Tuple $t): Tuple
    {
        return self::mul(1 / $f, $t);
    }

    public function reflect(Tuple $in): Tuple
    {
        return self::sub($in, self::mul(2.0 * self::dot($in, $this), $this));
    }

    public function normalize(): Tuple
    {
        return self::div($this->magnitude(), $this);
    }

    public function magnitude(): float
    {
        return sqrt($this->x ** 2 + $this->y ** 2 + $this->z ** 2 + $this->w ** 2);
    }

    public function x(): float
    {
        return $this->x;
    }

    public function y(): float
    {
        return $this->y;
    }

    public function z(): float
    {
        return $this->z;
    }

    public function w(): float
    {
        return $this->w;
    }
}
