<?php

declare(strict_types=1);

namespace UMA\Raytracer\Core;

/**
 * I would rather name this interface "Object", but it is a reserved keyword.
 */
interface Shape
{
    /**
     * Returns an ordered array of distances at which the
     * given ray intersects with the Shape.
     */
    public function intersect(Ray $r): Intersections;

    public function getMaterial(): Material;

    public function setMaterial(Material $material): void;

    public function getTransformation(): Matrix;

    public function setTransformation(Matrix $transformation): void;

    public function normalAt(Tuple $point);
}
