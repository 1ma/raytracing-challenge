<?php

declare(strict_types=1);

namespace UMA\Raytracer\Core;

final class Material
{
    /** @var Tuple */
    private $color;

    /** @var float */
    private $ambient;

    /** @var float */
    private $diffuse;

    /** @var float */
    private $specular;

    /** @var float */
    private $shininess;

    public function __construct(Tuple $color, float $ambient, float $diffuse, float $specular, float $shininess)
    {
        $this->color = $color;
        $this->ambient = $ambient;
        $this->diffuse = $diffuse;
        $this->specular = $specular;
        $this->shininess = $shininess;
    }

    public static function default(): Material
    {
        return new self(Tuple::color(1.0, 1.0, 1.0), 0.1, 0.9, 0.9, 200.0);
    }

    public function lightning(PointLight $light, Tuple $point, Tuple $eyeVector, Tuple $normalVector): Tuple
    {
        $effectiveColor = Tuple::hadamard($this->color(), $light->intensity());
        $lightVector = Tuple::sub($light->position(), $point)->normalize();
        $ambient = Tuple::mul($this->ambient(), $effectiveColor);

        $lightDotNormal = Tuple::dot($lightVector, $normalVector);
        if ($lightDotNormal < 0.0) {
            return $ambient;
        }

        $diffuse = Tuple::mul($lightDotNormal * $this->diffuse(), $effectiveColor);
        $reflectVector = $normalVector->reflect(Tuple::mul(-1.0, $lightVector));

        $reflectDotEye = Tuple::dot($reflectVector, $eyeVector);
        if ($reflectDotEye <= 0.0) {
            return Tuple::add($ambient, $diffuse);
        }

        $factor = $reflectDotEye ** $this->shininess();
        $specular = Tuple::mul($this->specular() * $factor, $light->intensity());

        return Tuple::add($ambient, Tuple::add($diffuse, $specular));
    }

    public function color(): Tuple
    {
        return $this->color;
    }

    public function ambient(): float
    {
        return $this->ambient;
    }

    public function diffuse(): float
    {
        return $this->diffuse;
    }

    public function specular(): float
    {
        return $this->specular;
    }

    public function shininess(): float
    {
        return $this->shininess;
    }
}
