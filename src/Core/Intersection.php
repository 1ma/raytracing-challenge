<?php

declare(strict_types=1);

namespace UMA\Raytracer\Core;

final class Intersection
{
    /** @var float */
    private $t;

    /** @var Shape */
    private $s;

    public function __construct(float $t, Shape $s)
    {
        $this->t = $t;
        $this->s = $s;
    }

    public function t(): float
    {
        return $this->t;
    }

    public function shape(): Shape
    {
        return $this->s;
    }
}
