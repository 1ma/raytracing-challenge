<?php

declare(strict_types=1);

namespace UMA\Raytracer\Core;

use function count;
use function usort;

final class Intersections
{
    /** @var Intersection[] */
    private $intersections;

    public function __construct(Intersection ...$intersections)
    {
        usort($intersections, [self::class, 'sortFunc']);

        $this->intersections = $intersections;
    }

    public function count(): int
    {
        return count($this->intersections);
    }

    public function get(int $i): Intersection
    {
        return $this->intersections[$i];
    }

    public function hit(): ?Intersection
    {
        foreach ($this->intersections as $intersection) {
            if ($intersection->t() > 0.0) {
                return $intersection;
            }
        }

        return null;
    }

    private static function sortFunc(Intersection $a, Intersection $b): int
    {
        if (Floats::equivalent($a->t(), $b->t())) {
            return 0;
        }

        return $a->t() < $b->t() ? -1 : 1;
    }
}
