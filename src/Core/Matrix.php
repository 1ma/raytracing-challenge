<?php

declare(strict_types=1);

namespace UMA\Raytracer\Core;

use function assert;
use function cos;
use function count;
use function implode;
use function sin;

final class Matrix
{
    /** @var int */
    private $dimension;

    /** @var float[] */
    private $matrix;

    public function __construct(int $dimension, float ...$values)
    {
        assert(count($values) === $dimension * $dimension);

        $this->dimension = $dimension;

        $this->matrix = $values;
    }

    public static function identity(): Matrix
    {
        return new self(4,
            1.0, 0.0, 0.0, 0.0,
            0.0, 1.0, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            0.0, 0.0, 0.0, 1.0
        );
    }

    public static function translation(float $x, float $y, float $z): Matrix
    {
        return new self(4,
            1.0, 0.0, 0.0,  $x,
            0.0, 1.0, 0.0,  $y,
            0.0, 0.0, 1.0,  $z,
            0.0, 0.0, 0.0, 1.0
        );
    }

    public static function scaling(float $x, float $y, float $z): Matrix
    {
        return new self(4,
             $x, 0.0, 0.0, 0.0,
            0.0,  $y, 0.0, 0.0,
            0.0, 0.0,  $z, 0.0,
            0.0, 0.0, 0.0, 1.0
        );
    }

    public static function rotationX(float $r): Matrix
    {
        return new self(4,
            1.0,     0.0,      0.0, 0.0,
            0.0, cos($r), -sin($r), 0.0,
            0.0, sin($r),  cos($r), 0.0,
            0.0,     0.0,      0.0, 1.0
        );
    }

    public static function rotationY(float $r): Matrix
    {
        return new self(4,
             cos($r), 0.0, sin($r), 0.0,
                 0.0, 1.0,     0.0, 0.0,
            -sin($r), 0.0, cos($r), 0.0,
                 0.0, 0.0,     0.0, 1.0
        );
    }

    public static function rotationZ(float $r): Matrix
    {
        return new self(4,
            cos($r), -sin($r), 0.0, 0.0,
            sin($r),  cos($r), 0.0, 0.0,
                0.0,      0.0, 1.0, 0.0,
                0.0,      0.0, 0.0, 1.0
        );
    }

    public static function shearing(float $xy, float $xz, float $yx, float $yz, float $zx, float $zy): Matrix
    {
        return new self(4,
            1.0, $xy, $xz, 0.0,
            $yx, 1.0, $yz, 0.0,
            $zx, $zy, 1.0, 0.0,
            0.0, 0.0, 0.0, 1.0
        );
    }

    public function get(int $row, int $column): float
    {
        assert($row < $this->dimension && $column < $this->dimension);

        return $this->matrix[$this->dimension * $row + $column];
    }

    public function inverse(): Matrix
    {
        assert(4 === $this->dimension && $this->invertible());

        $determinant = $this->determinant();

        $I = new self(4, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

        for ($r = 0; $r < $this->dimension; $r++) {
            for ($c = 0; $c < $this->dimension; $c++) {
                $I->matrix[$this->dimension * $c + $r] = $this->cofactor($r, $c) / $determinant;
            }
        }

        return $I;
    }

    public function invertible(): bool
    {
        return !Floats::equivalent(0.0, $this->determinant());
    }

    public function determinant(): float
    {
        assert(1 < $this->dimension);

        if (2 === $this->dimension) {
            return $this->matrix[0] * $this->matrix[3] - $this->matrix[1] * $this->matrix[2];
        }

        $determinant = 0.0;

        for ($c = 0; $c < $this->dimension; $c++) {
            $determinant += $this->matrix[$c] * $this->cofactor(0, $c);
        }

        return $determinant;
    }

    public function submatrix(int $row, int $column): Matrix
    {
        assert(3 === $this->dimension || 4 === $this->dimension);
        assert($row < $this->dimension && $column < $this->dimension);

        // In practice this method is only called on 3x3 and 4x4 Matrices
        $subMatrix = 3 === $this->dimension ?
            new self(2, 0.0, 0.0, 0.0, 0.0) :
            new self(3, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

        $i = 0;
        for ($r = 0; $r < $this->dimension; $r++) {
            for ($c = 0; $c < $this->dimension; $c++) {
                if ($r !== $row && $c !== $column) {
                    $subMatrix->matrix[$i++] = $this->matrix[$this->dimension * $r + $c];
                }
            }
        }

        return $subMatrix;
    }

    public function minor(int $row, int $column): float
    {
        assert($row < $this->dimension && $column < $this->dimension);

        return $this->submatrix($row, $column)->determinant();
    }

    public function cofactor(int $row, int $column): float
    {
        assert($row < $this->dimension && $column < $this->dimension);

        $multiplier = ($row + $column) % 2 ?
            -1 : 1;

        return $multiplier * $this->minor($row, $column);
    }

    public function transpose(): Matrix
    {
        assert(4 === $this->dimension);

        return new self(4,
            $this->matrix[0], $this->matrix[4],  $this->matrix[8], $this->matrix[12],
            $this->matrix[1], $this->matrix[5],  $this->matrix[9], $this->matrix[13],
            $this->matrix[2], $this->matrix[6], $this->matrix[10], $this->matrix[14],
            $this->matrix[3], $this->matrix[7], $this->matrix[11], $this->matrix[15]
        );
    }

    public static function equals(Matrix $A, Matrix $B): bool
    {
        if ($A->dimension !== $B->dimension) {
            return false;
        }

        foreach ($A->matrix as $i => $_) {
            if (!Floats::equivalent($A->matrix[$i], $B->matrix[$i])) {
                return false;
            }
        }

        return true;
    }

    public function apply(Matrix $other): Matrix
    {
        return self::mulMatrix($other, $this);
    }

    /**
     * The book claims we only need to support the multiplication
     * of 4x4 matrices, so for now I'm cutting corners on the algorithm.
     */
    public static function mulMatrix(Matrix $A, Matrix $B): Matrix
    {
        assert(4 === $A->dimension && 4 === $B->dimension);

        $AxB = new self(4, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

        for ($r = 0; $r < 4; $r++) {
            for ($c = 0; $c < 4; $c++) {
                $AxB->matrix[$AxB->dimension * $r + $c] =
                    $A->matrix[$A->dimension * $r    ] * $B->matrix[                    $c] +
                    $A->matrix[$A->dimension * $r + 1] * $B->matrix[$B->dimension * 1 + $c] +
                    $A->matrix[$A->dimension * $r + 2] * $B->matrix[$B->dimension * 2 + $c] +
                    $A->matrix[$A->dimension * $r + 3] * $B->matrix[$B->dimension * 3 + $c];
            }
        }

        return $AxB;
    }

    public static function mulTuple(Matrix $A, Tuple $b): Tuple
    {
        assert(4 === $A->dimension);

        return new Tuple(
             $A->matrix[0] * $b->x() +  $A->matrix[1] * $b->y() +  $A->matrix[2] * $b->z() +  $A->matrix[3] * $b->w(),
             $A->matrix[4] * $b->x() +  $A->matrix[5] * $b->y() +  $A->matrix[6] * $b->z() +  $A->matrix[7] * $b->w(),
             $A->matrix[8] * $b->x() +  $A->matrix[9] * $b->y() + $A->matrix[10] * $b->z() + $A->matrix[11] * $b->w(),
            $A->matrix[12] * $b->x() + $A->matrix[13] * $b->y() + $A->matrix[14] * $b->z() + $A->matrix[15] * $b->w()
        );
    }

    public function __toString()
    {
        return '| ' . implode(' | ', $this->matrix) . " |\n";
    }
}
