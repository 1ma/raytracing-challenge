<?php

declare(strict_types=1);

namespace UMA\Raytracer\Core;

use SplFixedArray;
use function max;
use function min;
use function round;
use function sprintf;
use function strlen;

final class Canvas
{
    /** @var int */
    private $width;

    /** @var int */
    private $height;

    /** @var Tuple[] */
    private $canvas;

    public function __construct(int $width, int $height, Tuple $defaultColor = null)
    {
        $this->width = $width;
        $this->height = $height;

        $defaultColor = $defaultColor ?? Tuple::color(0.0, 0.0, 0.0);

        $size = $this->height * $this->width;
        $this->canvas = new SplFixedArray($size);

        for ($i = 0; $i < $this->canvas->getSize(); $i++) {
            $this->canvas[$i] = $defaultColor;
        }
    }

    public function width(): int
    {
        return $this->width;
    }

    public function height(): int
    {
        return $this->height;
    }

    public function getPixel(int $width, int $height): Tuple
    {
        return $this->canvas[$this->width * $height + $width];
    }

    public function setPixel(int $width, int $height, Tuple $color): void
    {
        $this->canvas[$this->width * $height + $width] = $color;
    }

    public function toPPM(): string
    {
        $serialization = sprintf("P3\n%d %d\n255", $this->width, $this->height);
        $currentLineLength = 0;

        for ($i = 0; $i < $this->canvas->getSize(); $i++) {
            foreach ([$this->canvas[$i]->x(), $this->canvas[$i]->y(), $this->canvas[$i]->z()] as $component => $value) {
                $nextByte = (string) round(min(1.0, max(0.0, $value)) * 255);
                $nextByteLength = strlen($nextByte);

                // start a new line when we reach the first pixel of the next row,
                // or when we are about to go over 70 bytes on the current line.
                if ((0 === $i % $this->width && 0 === $component) || ($currentLineLength + 1 + $nextByteLength  > 70)) {
                    $serialization .= "\n" . $nextByte;
                    $currentLineLength = $nextByteLength;

                    continue;
                }

                $serialization .= ' ' . $nextByte;
                $currentLineLength += 1 + $nextByteLength;
            }
        }

        return $serialization . "\n";
    }
}
