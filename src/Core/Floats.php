<?php

declare(strict_types=1);

namespace UMA\Raytracer\Core;

use function abs;

/**
 * Pure functions that operate on floats.
 */
final class Floats
{
    private const CMP_EPSILON = 0.00001;

    /**
     * The book defines float equivalence as |a-b| < ε
     * being ε defined as the arbitrary value 0.00001
     */
    public static function equivalent(float $a, float $b): bool
    {
        return abs($a - $b) < self::CMP_EPSILON;
    }
}
