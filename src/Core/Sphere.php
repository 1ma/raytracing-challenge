<?php

declare(strict_types=1);

namespace UMA\Raytracer\Core;

use function sqrt;

final class Sphere implements Shape
{
    /** @var Tuple */
    private static $origin;

    /** @var Material */
    private $material;

    /** @var Matrix */
    private $inverse;

    public function __construct()
    {
        if (null === self::$origin) {
            self::$origin = Tuple::point(0.0, 0.0, 0.0);
        }

        $this->material = Material::default();
        $this->inverse = Matrix::identity();
    }

    public function intersect(Ray $r): Intersections
    {
        $transformedRay = $r->transform($this->inverse);

        $sphereToRayVector = Tuple::sub($transformedRay->origin(), self::$origin);

        $a = 2 * Tuple::dot($transformedRay->direction(), $transformedRay->direction());
        $b = 2 * Tuple::dot($transformedRay->direction(), $sphereToRayVector);
        $c = Tuple::dot($sphereToRayVector, $sphereToRayVector) - 1;

        $discriminant = ($b ** 2) - (2 * $a * $c);

        if ($discriminant < 0) {
            return new Intersections();
        }

        return new Intersections(
            new Intersection((- $b - sqrt($discriminant)) / $a, $this),
            new Intersection((- $b + sqrt($discriminant)) / $a, $this)
        );
    }

    public function getMaterial(): Material
    {
        return $this->material;
    }

    public function setMaterial(Material $material): void
    {
        $this->material = $material;
    }

    public function getTransformation(): Matrix
    {
        return $this->inverse->inverse();
    }

    public function setTransformation(Matrix $transformation): void
    {
        $this->inverse = $transformation->inverse();
    }

    public function normalAt(Tuple $point): Tuple
    {
        $shapePoint = Matrix::mulTuple($this->inverse, $point);
        $shapeNormal = Tuple::sub($shapePoint, self::$origin);
        $worldNormal = Matrix::mulTuple($this->inverse->transpose(), $shapeNormal);

        return Tuple::vector($worldNormal->x(), $worldNormal->y(), $worldNormal->z())->normalize();
    }
}
