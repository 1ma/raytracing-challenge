<?php

declare(strict_types=1);

namespace UMA\Raytracer\Tests\Book;

use UMA\Raytracer\Core\Material;
use UMA\Raytracer\Core\Matrix;
use UMA\Raytracer\Core\Ray;
use UMA\Raytracer\Core\Sphere;
use UMA\Raytracer\Core\Tuple;
use UMA\Raytracer\Tests\BookTestCase;

/**
 * @testdox SpheresFeature, part of Chapter 5: Ray-Sphere Intersections and Chapter 6: Light and Shading
 */
final class SpheresFeature extends BookTestCase
{
    /**
     * @testdox A ray intersects a sphere at two points
     */
    public function testScenario1(): void
    {
        $r = new Ray(Tuple::point(0.0, 0.0, -5.0), Tuple::vector(0.0, 0.0, 1.0));
        $s = new Sphere();

        $xs = $s->intersect($r);

        self::assertSame(2, $xs->count());
        self::assertSameFloat(4.0, $xs->get(0)->t());
        self::assertSameFloat(6.0, $xs->get(1)->t());
    }

    /**
     * @testdox A ray intersects a sphere at a tangent
     */
    public function testScenario2(): void
    {
        $r = new Ray(Tuple::point(0.0, 1.0, -5.0), Tuple::vector(0.0, 0.0, 1.0));
        $s = new Sphere();

        $xs = $s->intersect($r);

        self::assertSame(2, $xs->count());
        self::assertSameFloat(5.0, $xs->get(0)->t());
        self::assertSameFloat(5.0, $xs->get(1)->t());
    }

    /**
     * @testdox A ray misses a sphere
     */
    public function testScenario3(): void
    {
        $r = new Ray(Tuple::point(0.0, 2.0, -5.0), Tuple::vector(0.0, 0.0, 1.0));
        $s = new Sphere();

        $xs = $s->intersect($r);

        self::assertSame(0, $xs->count());
    }

    /**
     * @testdox A ray originates inside a a sphere
     */
    public function testScenario4(): void
    {
        $r = new Ray(Tuple::point(0.0, 0.0, 0.0), Tuple::vector(0.0, 0.0, 1.0));
        $s = new Sphere();

        $xs = $s->intersect($r);

        self::assertSame(2, $xs->count());
        self::assertSameFloat(-1.0, $xs->get(0)->t());
        self::assertSameFloat(1.0, $xs->get(1)->t());
    }

    /**
     * @testdox A sphere is behind a ray
     */
    public function testScenario5(): void
    {
        $r = new Ray(Tuple::point(0.0, 0.0, 5.0), Tuple::vector(0.0, 0.0, 1.0));
        $s = new Sphere();

        $xs = $s->intersect($r);

        self::assertSame(2, $xs->count());
        self::assertSameFloat(-6.0, $xs->get(0)->t());
        self::assertSameFloat(-4.0, $xs->get(1)->t());
    }

    /**
     * @testdox A sphere's default transformation
     */
    public function testScenario6(): void
    {
        $s = new Sphere();

        self::assertSameMatrix(Matrix::identity(), $s->getTransformation());
    }

    /**
     * @testdox Changing a sphere's transformation
     */
    public function testScenario7(): void
    {
        $s = new Sphere();
        $t = Matrix::translation(2.0, 3.0, 4.0);

        $s->setTransformation($t);

        self::assertSameMatrix($t, $s->getTransformation());
    }

    /**
     * @testdox Intersecting a scaled sphere with a ray
     */
    public function testScenario8(): void
    {
        $r = new Ray(Tuple::point(0.0, 0.0, -5.0), Tuple::vector(0.0, 0.0, 1.0));
        $s = new Sphere();

        $s->setTransformation(Matrix::scaling(2.0, 2.0, 2.0));
        $xs = $s->intersect($r);

        self::assertSame(2, $xs->count());
        self::assertSameFloat(3.0, $xs->get(0)->t());
        self::assertSameFloat(7.0, $xs->get(1)->t());
    }

    /**
     * @testdox Intersecting a translated sphere with a ray
     */
    public function testScenario9(): void
    {
        $r = new Ray(Tuple::point(0.0, 0.0, -5.0), Tuple::vector(0.0, 0.0, 1.0));
        $s = new Sphere();

        $s->setTransformation(Matrix::translation(5.0, 0.0, 0.0));
        $xs = $s->intersect($r);

        self::assertSame(0, $xs->count());
    }

    /**
     * @testdox The normal on a sphere at a point on the x axis
     */
    public function testScenario10(): void
    {
        $s = new Sphere();
        $n = $s->normalAt(Tuple::point(1.0, 0.0, 0.0));

        self::assertSameTuple(Tuple::vector(1.0, 0.0, 0.0), $n);
    }

    /**
     * @testdox The normal on a sphere at a point on the y axis
     */
    public function testScenario11(): void
    {
        $s = new Sphere();
        $n = $s->normalAt(Tuple::point(0.0, 1.0, 0.0));

        self::assertSameTuple(Tuple::vector(0.0, 1.0, 0.0), $n);
    }

    /**
     * @testdox The normal on a sphere at a point on the z axis
     */
    public function testScenario12(): void
    {
        $s = new Sphere();
        $n = $s->normalAt(Tuple::point(0.0, 0.0, 1.0));

        self::assertSameTuple(Tuple::vector(0.0, 0.0, 1.0), $n);
    }

    /**
     * @testdox The normal on a sphere at a nonaxial point
     */
    public function testScenario13(): void
    {
        $s = new Sphere();
        $n = $s->normalAt(Tuple::point(\sqrt(3.0) / 3.0, \sqrt(3.0) / 3.0, \sqrt(3.0) / 3.0));

        self::assertSameTuple(Tuple::vector(\sqrt(3.0) / 3.0, \sqrt(3.0) / 3.0, \sqrt(3.0) / 3.0), $n);
    }

    /**
     * @testdox The normal is a normalized vector
     */
    public function testScenario14(): void
    {
        $s = new Sphere();
        $n = $s->normalAt(Tuple::point(\sqrt(3.0) / 3.0, \sqrt(3.0) / 3.0, \sqrt(3.0) / 3.0));

        self::assertSameTuple($n, $n->normalize());
    }

    /**
     * @testdox Computing the normal on a translated sphere
     */
    public function testScenario15(): void
    {
        $s = new Sphere();
        $s->setTransformation(Matrix::translation(0.0, 1.0, 0.0));
        $n = $s->normalAt(Tuple::point(0.0, 1.70711, -0.70711));

        self::assertSameTuple(Tuple::vector(0.0, 0.70711, -0.70711), $n);
    }

    /**
     * @testdox Computing the normal on a transformed sphere
     */
    public function testScenario16(): void
    {
        $s = new Sphere();
        $m = Matrix::rotationZ(M_PI / 5.0)->apply(Matrix::scaling(1.0, 0.5, 1.0));
        $s->setTransformation($m);

        $n = $s->normalAt(Tuple::point(0.0, \sqrt(2.0) / 2.0, -\sqrt(2.0) / 2.0));

        self::assertSameTuple(Tuple::vector(0.0, 0.97014, -0.24254), $n);
    }

    /**
     * @testdox A sphere has a default material
     */
    public function testScenario17(): void
    {
        $s = new Sphere();
        $m = $s->getMaterial();

        self::assertEquals(Material::default(), $m);
    }

    /**
     * @testdox A sphere may be assigned a material
     */
    public function testScenario18(): void
    {
        $s = new Sphere();
        $m = new Material(Tuple::color(1.0, 1.0, 1.0), 1.0, 0.9, 0.9, 200.0);

        $s->setMaterial($m);

        self::assertEquals($m, $s->getMaterial());
    }
}
