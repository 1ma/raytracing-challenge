<?php

declare(strict_types=1);

namespace UMA\Raytracer\Tests\Book;

use UMA\Raytracer\Core\Matrix;
use UMA\Raytracer\Core\Tuple;
use UMA\Raytracer\Tests\BookTestCase;

/**
 *  @testdox TransformationsFeature, Chapter 4: Matrix Transformations
 */
final class TransformationsFeature extends BookTestCase
{
    /**
     * @testdox Multiplying by a translation matrix
     */
    public function testScenario1(): void
    {
        $transform = Matrix::translation(5.0, -3.0, 2.0);
        $p = Tuple::point(-3.0, 4.0, 5.0);

        self::assertSameTuple(Tuple::point(2.0, 1.0, 7.0), Matrix::mulTuple($transform, $p));
    }

    /**
     * @testdox Multiplying by the inverse of a translation matrix
     */
    public function testScenario2(): void
    {
        $transform = Matrix::translation(5.0, -3.0, 2.0);
        $inv = $transform->inverse();
        $p = Tuple::point(-3.0, 4.0, 5.0);

        self::assertSameTuple(Tuple::point(-8.0, 7.0, 3.0), Matrix::mulTuple($inv, $p));
    }

    /**
     * @testdox Translation does not affect vectors
     */
    public function testScenario3(): void
    {
        $transform = Matrix::translation(5.0, -3.0, 2.0);
        $v = Tuple::vector(-3.0, 4.0, 5.0);

        self::assertSameTuple($v, Matrix::mulTuple($transform, $v));
    }

    /**
     * @testdox A scaling matrix applied to a point
     */
    public function testScenario4(): void
    {
        $transform = Matrix::scaling(2.0, 3.0, 4.0);
        $p = Tuple::point(-4.0, 6.0, 8.0);

        self::assertSameTuple(Tuple::point(-8.0, 18.0, 32.0), Matrix::mulTuple($transform, $p));
    }

    /**
     * @testdox A scaling matrix applied to a vector
     */
    public function testScenario5(): void
    {
        $transform = Matrix::scaling(2.0, 3.0, 4.0);
        $v = Tuple::vector(-4.0, 6.0, 8.0);

        self::assertSameTuple(Tuple::vector(-8.0, 18.0, 32.0), Matrix::mulTuple($transform, $v));
    }

    /**
     * @testdox Multiplying by the inverse of a scaling matrix
     */
    public function testScenario6(): void
    {
        $transform = Matrix::scaling(2.0, 3.0, 4.0);
        $inv = $transform->inverse();
        $v = Tuple::vector(-4.0, 6.0, 8.0);

        self::assertSameTuple(Tuple::vector(-2.0, 2.0, 2.0), Matrix::mulTuple($inv, $v));
    }

    /**
     * @testdox Reflection is scaling by a negative value
     */
    public function testScenario7(): void
    {
        $transform = Matrix::scaling(-1.0, 1.0, 1.0);
        $p = Tuple::point(2.0, 3.0, 4.0);

        self::assertSameTuple(Tuple::point(-2.0, 3.0, 4.0), Matrix::mulTuple($transform, $p));
    }

    /**
     * @testdox Rotating a point around the x axis
     */
    public function testScenario8(): void
    {
        $p = Tuple::point(0.0, 1.0, 0.0);
        $halfQuarter = Matrix::rotationX(M_PI_4);
        $fullQuarter = Matrix::rotationX(M_PI_2);

        self::assertSameTuple(Tuple::point(0.0, \sqrt(2) / 2.0, \sqrt(2) / 2.0), Matrix::mulTuple($halfQuarter, $p));
        self::assertSameTuple(Tuple::point(0.0, 0.0, 1.0), Matrix::mulTuple($fullQuarter, $p));
    }

    /**
     * @testdox The inverse of an x-rotation rotates in the opposite direction
     */
    public function testScenario9(): void
    {
        $p = Tuple::point(0.0, 1.0, 0.0);
        $halfQuarter = Matrix::rotationX(M_PI_4);
        $inv = $halfQuarter->inverse();

        self::assertSameTuple(Tuple::point(0.0, \sqrt(2) / 2.0, -\sqrt(2) / 2.0), Matrix::mulTuple($inv, $p));
    }

    /**
     * @testdox Rotating a point around the y axis
     */
    public function testScenario10(): void
    {
        $p = Tuple::point(0.0, 0.0, 1.0);
        $halfQuarter = Matrix::rotationY(M_PI_4);
        $fullQuarter = Matrix::rotationY(M_PI_2);

        self::assertSameTuple(Tuple::point(\sqrt(2) / 2.0, 0.0, \sqrt(2) / 2.0), Matrix::mulTuple($halfQuarter, $p));
        self::assertSameTuple(Tuple::point(1.0, 0.0, 0.0), Matrix::mulTuple($fullQuarter, $p));
    }

    /**
     * @testdox Rotating a point around the z axis
     */
    public function testScenario11(): void
    {
        $p = Tuple::point(0.0, 1.0, 0.0);
        $halfQuarter = Matrix::rotationZ(M_PI_4);
        $fullQuarter = Matrix::rotationZ(M_PI_2);

        self::assertSameTuple(Tuple::point(-\sqrt(2) / 2.0, \sqrt(2) / 2.0, 0.0), Matrix::mulTuple($halfQuarter, $p));
        self::assertSameTuple(Tuple::point(-1.0, 0.0, 0.0), Matrix::mulTuple($fullQuarter, $p));
    }

    /**
     * @testdox A shearing transformation moves x in proportion to y
     */
    public function testScenario12(): void
    {
        $transform = Matrix::shearing(1.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        $p = Tuple::point(2.0, 3.0, 4.0);

        self::assertSameTuple(Tuple::point(5.0, 3.0, 4.0), Matrix::mulTuple($transform, $p));
    }

    /**
     * @testdox A shearing transformation moves x in proportion to z
     */
    public function testScenario13(): void
    {
        $transform = Matrix::shearing(0.0, 1.0, 0.0, 0.0, 0.0, 0.0);
        $p = Tuple::point(2.0, 3.0, 4.0);

        self::assertSameTuple(Tuple::point(6.0, 3.0, 4.0), Matrix::mulTuple($transform, $p));
    }

    /**
     * @testdox A shearing transformation moves y in proportion to x
     */
    public function testScenario14(): void
    {
        $transform = Matrix::shearing(0.0, 0.0, 1.0, 0.0, 0.0, 0.0);
        $p = Tuple::point(2.0, 3.0, 4.0);

        self::assertSameTuple(Tuple::point(2.0, 5.0, 4.0), Matrix::mulTuple($transform, $p));
    }

    /**
     * @testdox A shearing transformation moves y in proportion to z
     */
    public function testScenario15(): void
    {
        $transform = Matrix::shearing(0.0, 0.0, 0.0, 1.0, 0.0, 0.0);
        $p = Tuple::point(2.0, 3.0, 4.0);

        self::assertSameTuple(Tuple::point(2.0, 7.0, 4.0), Matrix::mulTuple($transform, $p));
    }

    /**
     * @testdox A shearing transformation moves z in proportion to x
     */
    public function testScenario16(): void
    {
        $transform = Matrix::shearing(0.0, 0.0, 0.0, 0.0, 1.0, 0.0);
        $p = Tuple::point(2.0, 3.0, 4.0);

        self::assertSameTuple(Tuple::point(2.0, 3.0, 6.0), Matrix::mulTuple($transform, $p));
    }

    /**
     * @testdox A shearing transformation moves z in proportion to y
     */
    public function testScenario17(): void
    {
        $transform = Matrix::shearing(0.0, 0.0, 0.0, 0.0, 0.0, 1.0);
        $p = Tuple::point(2.0, 3.0, 4.0);

        self::assertSameTuple(Tuple::point(2.0, 3.0, 7.0), Matrix::mulTuple($transform, $p));
    }

    /**
     * @testdox Individual transformations are applied in sequence
     */
    public function testScenario18(): void
    {
        $p = Tuple::point(1.0, 0.0, 1.0);
        $A = Matrix::rotationX(M_PI_2);
        $B = Matrix::scaling(5.0, 5.0, 5.0);
        $C = Matrix::translation(10.0, 5.0, 7.0);

        $p2 = Matrix::mulTuple($A, $p);

        self::assertSameTuple(Tuple::point(1.0, -1.0, 0.0), $p2);

        $p3 = Matrix::mulTuple($B, $p2);

        self::assertSameTuple(Tuple::point(5.0, -5.0, 0.0), $p3);

        $p4 = Matrix::mulTuple($C, $p3);

        self::assertSameTuple(Tuple::point(15.0, 0.0, 7.0), $p4);
    }

    /**
     * @testdox Chained transformations must be applied in reverse order
     */
    public function testScenario19(): void
    {
        $p = Tuple::point(1.0, 0.0, 1.0);
        $A = Matrix::rotationX(M_PI_2);
        $B = Matrix::scaling(5.0, 5.0, 5.0);
        $C = Matrix::translation(10.0, 5.0, 7.0);

        $T = Matrix::mulMatrix($C, Matrix::mulMatrix($B, $A));

        self::assertSameTuple(Tuple::point(15.0, 0.0, 7.0), Matrix::mulTuple($T, $p));

        // This part of the test is mine, the book suggests but does not mandate a fluent interface
        $T2 = Matrix::identity()->apply($A)->apply($B)->apply($C);

        self::assertSameTuple(Tuple::point(15.0, 0.0, 7.0), Matrix::mulTuple($T2, $p));
    }


}
