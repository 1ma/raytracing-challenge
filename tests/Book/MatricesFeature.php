<?php

declare(strict_types=1);

namespace UMA\Raytracer\Tests\Book;

use UMA\Raytracer\Core\Matrix;
use UMA\Raytracer\Core\Tuple;
use UMA\Raytracer\Tests\BookTestCase;

/**
 * @testdox MatricesFeature, Chapter 3: Matrices
 */
final class MatricesFeature extends BookTestCase
{
    /**
     * @testdox Constructing and inspecting a 4x4 matrix
     */
    public function testScenario1(): void
    {
        $M = new Matrix(4,
             1.0,  2.0,  3.0,  4.0,
             5.5,  6.5,  7.5,  8.5,
             9.0, 10.0, 11.0, 12.0,
            13.5, 14.5, 15.5, 16.5
        );

        self::assertSameFloat( 1.0, $M->get(0, 0));
        self::assertSameFloat( 4.0, $M->get(0, 3));
        self::assertSameFloat( 5.5, $M->get(1, 0));
        self::assertSameFloat( 7.5, $M->get(1, 2));
        self::assertSameFloat(11.0, $M->get(2, 2));
        self::assertSameFloat(13.5, $M->get(3, 0));
        self::assertSameFloat(15.5, $M->get(3, 2));
    }

    /**
     * @testdox A 2x2 matrix ought to be representable
     */
    public function testScenario2(): void
    {
        $M = new Matrix(2,
            -3.0,  5.0,
             1.0, -2.0
        );

        self::assertSameFloat(-3.0, $M->get(0, 0));
        self::assertSameFloat( 5.0, $M->get(0, 1));
        self::assertSameFloat( 1.0, $M->get(1, 0));
        self::assertSameFloat(-2.0, $M->get(1, 1));
    }

    /**
     * @testdox A 3x3 matrix ought to be representable
     */
    public function testScenario3(): void
    {
        $M = new Matrix(3,
            -3.0,  5.0,  0.0,
             1.0, -2.0, -7.0,
             0.0,  1.0,  1.0
        );

        self::assertSameFloat(-3.0, $M->get(0, 0));
        self::assertSameFloat(-2.0, $M->get(1, 1));
        self::assertSameFloat( 1.0, $M->get(2, 2));
    }

    /**
     * @testdox Matrix equality with identical matrices
     */
    public function testScenario4(): void
    {
        $A = new Matrix(4,
            1.0, 2.0, 3.0, 4.0,
            5.0, 6.0, 7.0, 8.0,
            9.0, 8.0, 7.0, 6.0,
            5.0, 4.0, 3.0, 2.0
        );

        $B = new Matrix(4,
            1.0, 2.0, 3.0, 4.0,
            5.0, 6.0, 7.0, 8.0,
            9.0, 8.0, 7.0, 6.0,
            5.0, 4.0, 3.0, 2.0
        );

        self::assertTrue(Matrix::equals($A, $B));
    }

    /**
     * @testdox Matrix equality with different matrices
     */
    public function testScenario5(): void
    {
        $A = new Matrix(4,
            1.0, 2.0, 3.0, 4.0,
            5.0, 6.0, 7.0, 8.0,
            9.0, 8.0, 7.0, 6.0,
            5.0, 4.0, 3.0, 2.0
        );

        $B = new Matrix(4,
            2.0, 3.0, 4.0, 5.0,
            6.0, 7.0, 8.0, 9.0,
            8.0, 7.0, 6.0, 5.0,
            4.0, 3.0, 2.0, 1.0
        );

        self::assertFalse(Matrix::equals($A, $B));
    }

    /**
     * @testdox Multiplying two matrices
     */
    public function testScenario6(): void
    {
        $A = new Matrix(4,
            1.0, 2.0, 3.0, 4.0,
            5.0, 6.0, 7.0, 8.0,
            9.0, 8.0, 7.0, 6.0,
            5.0, 4.0, 3.0, 2.0
        );

        $B = new Matrix(4,
            -2.0, 1.0, 2.0,  3.0,
             3.0, 2.0, 1.0, -1.0,
             4.0, 3.0, 6.0,  5.0,
             1.0, 2.0, 7.0,  8.0
        );

        $AxB = new Matrix(4,
            20.0, 22.0,  50.0,  48.0,
            44.0, 54.0, 114.0, 108.0,
            40.0, 58.0, 110.0, 102.0,
            16.0, 26.0,  46.0,  42.0
        );

        self::assertSameMatrix($AxB, Matrix::mulMatrix($A, $B));
    }

    /**
     * @testdox A matrix multiplied by a tuple
     */
    public function testScenario7(): void
    {
        $A = new Matrix(4,
            1.0, 2.0, 3.0, 4.0,
            2.0, 4.0, 4.0, 2.0,
            8.0, 6.0, 4.0, 1.0,
            0.0, 0.0, 0.0, 1.0
        );

        $b = new Tuple(1.0, 2.0, 3.0, 1.0);

        self::assertSameTuple(new Tuple(18.0, 24.0, 33.0, 1.0), Matrix::mulTuple($A, $b));
    }

    /**
     * @testdox Multiplying a matrix by the identity matrix
     */
    public function testScenario8(): void
    {
        $A = new Matrix(4,
            0.0, 1.0,  2.0,  4.0,
            1.0, 2.0,  4.0,  8.0,
            2.0, 4.0,  8.0, 16.0,
            4.0, 8.0, 16.0, 32.0
        );

        $I = new Matrix(4,
            1.0, 0.0, 0.0, 0.0,
            0.0, 1.0, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            0.0, 0.0, 0.0, 1.0
        );

        self::assertSameMatrix($A, Matrix::mulMatrix($A, $I));
    }

    /**
     * @testdox Transposing a matrix
     */
    public function testScenario9(): void
    {
        $A = new Matrix(4,
            0.0, 9.0, 3.0, 0.0,
            9.0, 8.0, 0.0, 8.0,
            1.0, 8.0, 5.0, 3.0,
            0.0, 0.0, 5.0, 8.0
        );

        $T = new Matrix(4,
            0.0, 9.0, 1.0, 0.0,
            9.0, 8.0, 8.0, 0.0,
            3.0, 0.0, 5.0, 5.0,
            0.0, 8.0, 3.0, 8.0
        );

        self::assertSameMatrix($T, $A->transpose());
    }

    /**
     * @testdox Calculating the determinant of a 2x2 matrix
     */
    public function testScenario10(): void
    {
        $A = new Matrix(2,
             1.0, 5.0,
            -3.0, 2.0
        );

        self::assertSameFloat(17.0, $A->determinant());
    }

    /**
     * @testdox A submatrix of a 3x3 matrix is a 2x2 matrix
     */
    public function testScenario11(): void
    {
        $A = new Matrix(3,
             1.0, 5.0,  0.0,
            -3.0, 2.0,  7.0,
             0.0, 6.0, -3.0
        );

        $S = new Matrix(2,
            -3.0, 2.0,
             0.0, 6.0
        );

        self::assertSameMatrix($S, $A->submatrix(0, 2));
    }

    /**
     * @testdox A submatrix of a 4x4 matrix is a 3x3 matrix
     */
    public function testScenario12(): void
    {
        $A = new Matrix(4,
            -6.0, 1.0,  1.0, 6.0,
            -8.0, 5.0,  8.0, 6.0,
            -1.0, 0.0,  8.0, 2.0,
            -7.0, 1.0, -1.0, 1.0
        );

        $S = new Matrix(3,
            -6.0,  1.0, 6.0,
            -8.0,  8.0, 6.0,
            -7.0, -1.0, 1.0
        );

        self::assertSameMatrix($S, $A->submatrix(2, 1));
    }

    /**
     * @testdox Calculating a minor of a 3x3 matrix
     */
    public function testScenario13(): void
    {
        $A = new Matrix(3,
            3.0,  5.0,  0.0,
            2.0, -1.0, -7.0,
            6.0, -1.0,  5.0
        );

        $B = $A->submatrix(1, 0);

        self::assertSameFloat(25.0, $B->determinant());
        self::assertSameFloat(25.0, $A->minor(1, 0));
    }

    /**
     * @testdox Calculating a cofactor of a 3x3 matrix
     */
    public function testScenario14(): void
    {
        $A = new Matrix(3,
            3.0,  5.0,  0.0,
            2.0, -1.0, -7.0,
            6.0, -1.0,  5.0
        );

        self::assertSameFloat(-12.0, $A->minor(0, 0));
        self::assertSameFloat(-12.0, $A->cofactor(0, 0));
        self::assertSameFloat( 25.0, $A->minor(1, 0));
        self::assertSameFloat(-25.0, $A->cofactor(1, 0));
    }

    /**
     * @testdox Calculating the determinant of a 3x3 matrix
     */
    public function testScenario15(): void
    {
        $A = new Matrix(3,
             1.0, 2.0,  6.0,
            -5.0, 8.0, -4.0,
             2.0, 6.0,  4.0
        );

        self::assertSameFloat(  56.0, $A->cofactor(0, 0));
        self::assertSameFloat(  12.0, $A->cofactor(0, 1));
        self::assertSameFloat( -46.0, $A->cofactor(0, 2));
        self::assertSameFloat(-196.0, $A->determinant());
    }

    /**
     * @testdox Calculating the determinant of a 4x4 matrix
     */
    public function testScenario16(): void
    {
        $A = new Matrix(4,
            -2.0, -8.0,  3.0,  5.0,
            -3.0,  1.0,  7.0,  3.0,
             1.0,  2.0, -9.0,  6.0,
            -6.0,  7.0,  7.0, -9.0
        );

        self::assertSameFloat(  690.0, $A->cofactor(0, 0));
        self::assertSameFloat(  447.0, $A->cofactor(0, 1));
        self::assertSameFloat(  210.0, $A->cofactor(0, 2));
        self::assertSameFloat(   51.0, $A->cofactor(0, 3));
        self::assertSameFloat(-4071.0, $A->determinant());
    }

    /**
     * @testdox Testing an invertible matrix for invertibility
     */
    public function testScenario17(): void
    {
        $A = new Matrix(4,
            6.0,  4.0, 4.0,  4.0,
            5.0,  5.0, 7.0,  6.0,
            4.0, -9.0, 3.0, -7.0,
            9.0,  1.0, 7.0, -6.0
        );

        self::assertSameFloat(-2120.0, $A->determinant());
        self::assertTrue($A->invertible());
    }

    /**
     * @testdox Testing a noninvertible matrix for invertibility
     */
    public function testScenario18(): void
    {
        $A = new Matrix(4,
            -4.0,  2.0, -2.0, -3.0,
             9.0,  6.0,  2.0,  6.0,
             0.0, -5.0,  1.0, -5.0,
             0.0,  0.0,  0.0,  0.0
        );

        self::assertSameFloat(0.0, $A->determinant());
        self::assertFalse($A->invertible());
    }

    /**
     * @testdox Calculating the inverse of a matrix
     */
    public function testScenario19(): void
    {
        $A = new Matrix(4,
            -5.0,  2.0,  6.0, -8.0,
             1.0, -5.0,  1.0,  8.0,
             7.0,  7.0, -6.0, -7.0,
             1.0, -3.0,  7.0,  4.0
        );

        $B = $A->inverse();

        $I = new Matrix(4,
             0.21805,  0.45113,  0.24060, -0.04511,
            -0.80827, -1.45677, -0.44361,  0.52068,
            -0.07895, -0.22368, -0.05263,  0.19737,
            -0.52256, -0.81391, -0.30075,  0.30639
        );

        self::assertSameFloat(532.0, $A->determinant());
        self::assertSameFloat(-160.0, $A->cofactor(2, 3));
        self::assertSameFloat(-160.0/532.0, $B->get(3, 2));
        self::assertSameFloat(105.0, $A->cofactor(3, 2));
        self::assertSameFloat(105.0/532.0, $B->get(2, 3));
        self::assertSameMatrix($I, $B);
    }

    /**
     * @testdox Calculating the inverse of another matrix
     */
    public function testScenario20(): void
    {
        $A = new Matrix(4,
             8.0, -5.0,  9.0,  2.0,
             7.0,  5.0,  6.0,  1.0,
            -6.0,  0.0,  9.0,  6.0,
            -3.0,  0.0, -9.0, -4.0
        );

        $I = new Matrix(4,
            -0.15385, -0.15385, -0.28205, -0.53846,
            -0.07692,  0.12308,  0.02564,  0.03077,
             0.35897,  0.35897,  0.43590,  0.92308,
            -0.69231, -0.69231, -0.76923, -1.92308
        );
        self::assertSameMatrix($I, $A->inverse());
    }

    /**
     * @testdox Calculating the inverse of a third matrix
     */
    public function testScenario21(): void
    {
        $A = new Matrix(4,
             9.0,  3.0,  0.0,  9.0,
            -5.0, -2.0, -6.0, -3.0,
            -4.0,  9.0,  6.0,  4.0,
            -7.0,  6.0,  6.0,  2.0
        );

        $I = new Matrix(4,
            -0.04074, -0.07778,  0.14444, -0.22222,
            -0.07778,  0.03333,  0.36667, -0.33333,
            -0.02901, -0.14630, -0.10926,  0.12963,
             0.17778,  0.06667, -0.26667,  0.33333
        );

        self::assertSameMatrix($I, $A->inverse());
    }

    /**
     * @testdox Multiplying a product by its inverse
     */
    public function testScenario22(): void
    {
        $A = new Matrix(4,
             3.0, -9.0,  7.0,  3.0,
             3.0, -8.0,  2.0, -9.0,
            -4.0,  4.0,  4.0,  1.0,
            -6.0,  5.0, -1.0,  1.0
        );

        $B = new Matrix(4,
            8.0,  2.0, 2.0, 2.0,
            3.0, -1.0, 7.0, 0.0,
            7.0,  0.0, 5.0, 4.0,
            6.0, -2.0, 0.0, 5.0
        );

        $C = Matrix::mulMatrix($A, $B);

        self::assertSameMatrix($A, Matrix::mulMatrix($C, $B->inverse()));
    }
}
