<?php

declare(strict_types=1);

namespace UMA\Raytracer\Tests\Book;

use UMA\Raytracer\Core\Material;
use UMA\Raytracer\Core\PointLight;
use UMA\Raytracer\Core\Tuple;
use UMA\Raytracer\Tests\BookTestCase;

/**
 * @testdox MaterialsFeature, Chapter 6: Light and Shading
 */
final class MaterialsFeature extends BookTestCase
{
    /**
     * @testdox A point light has a position and intensity
     */
    public function testScenario1(): void
    {
        $m = Material::default();

        self::assertSameTuple(Tuple::color(1.0, 1.0, 1.0), $m->color());
        self::assertSameFloat(  0.1, $m->ambient());
        self::assertSameFloat(  0.9, $m->diffuse());
        self::assertSameFloat(  0.9, $m->specular());
        self::assertSameFloat(200.0, $m->shininess());
    }

    /**
     * @testdox Lighting with the eye between the light and the surface
     */
    public function testScenario2(): void
    {
        $m = Material::default();
        $position = Tuple::point(0.0, 0.0, 0.0);

        $eyev = Tuple::vector(0.0, 0.0, -1.0);
        $normalv = Tuple::vector(0.0, 0.0, -1.0);
        $light = new PointLight(Tuple::color(1.0, 1.0, 1.0), Tuple::point(0.0, 0.0, -10.0));

        self::assertSameTuple(Tuple::color(1.9, 1.9, 1.9), $m->lightning($light, $position, $eyev, $normalv));
    }

    /**
     * @testdox Lighting with the eye between light and surface, eye offset 45°
     */
    public function testScenario3(): void
    {
        $m = Material::default();
        $position = Tuple::point(0.0, 0.0, 0.0);

        $eyev = Tuple::vector(0.0, \sqrt(2.0) / 2.0, -\sqrt(2.0) / 2.0);
        $normalv = Tuple::vector(0.0, 0.0, -1.0);
        $light = new PointLight( Tuple::color(1.0, 1.0, 1.0), Tuple::point(0.0, 0.0, -10.0));

        self::assertSameTuple(Tuple::color(1.0, 1.0, 1.0), $m->lightning($light, $position, $eyev, $normalv));
    }

    /**
     * @testdox Lighting with eye opposite surface, light offset 45°
     */
    public function testScenario4(): void
    {
        $m = Material::default();
        $position = Tuple::point(0.0, 0.0, 0.0);

        $eyev = Tuple::vector(0.0, 0.0, -1.0);
        $normalv = Tuple::vector(0.0, 0.0, -1.0);
        $light = new PointLight(Tuple::color(1.0, 1.0, 1.0), Tuple::point(0.0, 10.0, -10.0));

        self::assertSameTuple(Tuple::color(0.7364, 0.7364, 0.7364), $m->lightning($light, $position, $eyev, $normalv));
    }

    /**
     * @testdox Lighting with eye in the path of the reflection vector
     */
    public function testScenario5(): void
    {
        $m = Material::default();
        $position = Tuple::point(0.0, 0.0, 0.0);

        $eyev = Tuple::vector(0.0, -\sqrt(2.0) / 2.0, -\sqrt(2.0) / 2.0);
        $normalv = Tuple::vector(0.0, 0.0, -1.0);
        $light = new PointLight(Tuple::color(1.0, 1.0, 1.0), Tuple::point(0.0, 10.0, -10.0));

        self::assertSameTuple(Tuple::color(1.6364, 1.6364, 1.6364), $m->lightning($light, $position, $eyev, $normalv));
    }

    /**
     * @testdox Lighting with the light behind the surface
     */
    public function testScenario6(): void
    {
        $m = Material::default();
        $position = Tuple::point(0.0, 0.0, 0.0);

        $eyev = Tuple::vector(0.0, 0.0, -1.0);
        $normalv = Tuple::vector(0.0, 0.0, -1.0);
        $light = new PointLight(Tuple::color(1.0, 1.0, 1.0), Tuple::point(0.0, 0.0, 10.0));

        self::assertSameTuple(Tuple::color(0.1, 0.1, 0.1), $m->lightning($light, $position, $eyev, $normalv));
    }
}
