<?php

declare(strict_types=1);

namespace UMA\Raytracer\Tests\Book;

use UMA\Raytracer\Core\Tuple;
use UMA\Raytracer\Tests\BookTestCase;

/**
 * @testdox TuplesFeature, Chapter 1: Tuples, Points and Vectors and Chapter 2: Drawing on a Canvas
 */
final class TuplesFeature extends BookTestCase
{
    /**
     * @testdox A tuple with w=1.0 is a point
     */
    public function testScenario1(): void
    {
        $a = new Tuple(4.3, -4.2, 3.1, 1.0);

        self::assertSameFloat(4.3, $a->x());
        self::assertSameFloat(-4.2, $a->y());
        self::assertSameFloat(3.1, $a->z());
        self::assertSameFloat(1.0, $a->w());
    }

    /**
     * @testdox A tuple with w=0 is a vector
     */
    public function testScenario2(): void
    {
        $a = new Tuple(4.3, -4.2, 3.1, 0.0);

        self::assertSameFloat(4.3, $a->x());
        self::assertSameFloat(-4.2, $a->y());
        self::assertSameFloat(3.1, $a->z());
        self::assertSameFloat(0.0, $a->w()); // Note that in PHP 0 !== 0.0 (using strict comparison)
    }

    /**
     * @testdox point() creates tuples with w=1.0
     */
    public function testScenario3(): void
    {
        $p = Tuple::point(4.0, -4.0, 3.0);

        self::assertInstanceOf(Tuple::class, $p);
        self::assertSameFloat(1.0, $p->w());
    }

    /**
     * @testdox vector() creates tuples with w=0.0
     */
    public function testScenario4(): void
    {
        $v = Tuple::vector(4.0, -4.0, 3.0);

        self::assertInstanceOf(Tuple::class, $v);
        self::assertSameFloat(0.0, $v->w());
    }

    /**
     * @testdox Adding two tuples
     */
    public function testScenario5(): void
    {
        $a1 = new Tuple(3.0, -2.0, 5.0, 1.0);
        $a2 = new Tuple(-2.0, 3.0, 1.0, 0.0);

        $a3 = Tuple::add($a1, $a2);

        self::assertSameTuple(new Tuple(1.0, 1.0, 6.0, 1.0), $a3);
    }

    /**
     * @testdox Subtracting two points
     */
    public function testScenario6(): void
    {
        $p1 = Tuple::point(3.0, 2.0, 1.0);
        $p2 = Tuple::point(5.0, 6.0, 7.0);

        $v = Tuple::sub($p1, $p2);

        self::assertSameTuple(Tuple::vector(-2.0, -4.0, -6.0), $v);
    }

    /**
     * @testdox Subtracting a vector from a point
     */
    public function testScenario7(): void
    {
        $p = Tuple::point(3.0, 2.0, 1.0);
        $v = Tuple::vector(5.0, 6.0, 7.0);

        $p2 = Tuple::sub($p, $v);

        self::assertSameTuple(Tuple::point(-2.0, -4.0, -6.0), $p2);
    }

    /**
     * @testdox Subtracting two vectors
     */
    public function testScenario8(): void
    {
        $v1 = Tuple::vector(3.0, 2.0, 1.0);
        $v2 = Tuple::vector(5.0, 6.0, 7.0);

        $v3 = Tuple::sub($v1, $v2);

        self::assertSameTuple(Tuple::vector(-2.0, -4.0, -6.0), $v3);
    }

    /**
     * @testdox Subtracting a vector from the zero vector
     */
    public function testScenario9(): void
    {
        $zero = Tuple::vector(0.0, 0.0, 0.0);
        $v = Tuple::vector(1.0, -2.0, 3.0);

        $v2 = Tuple::sub($zero, $v);

        self::assertSameTuple(Tuple::vector(-1.0, 2.0, -3.0), $v2);
    }

    /**
     * @testdox Negating a tuple
     */
    public function testScenario10(): void
    {
        $a = new Tuple(1.0, -2.0, 3.0, -4.0);

        $a2 = Tuple::neg($a);

        self::assertSameTuple(new Tuple(-1.0, 2.0, -3.0, 4.0), $a2);
    }

    /**
     * @testdox Multiplying a tuple by a scalar
     */
    public function testScenario11(): void
    {
        $a = new Tuple(1.0, -2.0, 3.0, -4.0);

        $a2 = Tuple::mul(3.5, $a);

        self::assertSameTuple(new Tuple(3.5, -7.0, 10.5, -14.0), $a2);
    }

    /**
     * @testdox Multiplying a tuple by a fraction
     */
    public function testScenario12(): void
    {
        $a = new Tuple(1.0, -2.0, 3.0, -4.0);

        $a2 = Tuple::mul(0.5, $a);

        self::assertSameTuple(new Tuple(0.5, -1.0, 1.5, -2.0), $a2);
    }

    /**
     * @testdox Dividing a tuple by a scalar
     */
    public function testScenario13(): void
    {
        $a = new Tuple(1.0, -2.0, 3.0, -4.0);

        $a2 = Tuple::div(2, $a);

        self::assertSameTuple(new Tuple(0.5, -1.0, 1.5, -2.0), $a2);
    }

    /**
     * @testdox Computing the magnitude of several vectors
     */
    public function testScenario14(): void
    {
        self::assertSameFloat(1.0, Tuple::vector(1.0, 0.0, 0.0)->magnitude());
        self::assertSameFloat(1.0, Tuple::vector(0.0, 1.0, 0.0)->magnitude());
        self::assertSameFloat(1.0, Tuple::vector(0.0, 0.0, 1.0)->magnitude());
        self::assertSameFloat(\sqrt(14.0), Tuple::vector(1.0, 2.0, 3.0)->magnitude());
        self::assertSameFloat(\sqrt(14.0), Tuple::vector(-1.0, -2.0, -3.0)->magnitude());
    }

    /**
     * @testdox Normalizing vector(4,0,0) gives vector(1,0,0)
     */
    public function testScenario15(): void
    {
        self::assertSameTuple(Tuple::vector(1.0, 0.0, 0.0), Tuple::vector(4.0, 0.0, 0.0)->normalize());
    }


    /**
     * @testdox Normalizing vector(1,2,3)
     */
    public function testScenario16(): void
    {
        self::assertSameTuple(Tuple::vector(1.0 / \sqrt(14.0), 2.0 / \sqrt(14.0), 3.0 / \sqrt(14.0)), Tuple::vector(1.0, 2.0, 3.0)->normalize());
    }

    /**
     * @testdox The magnitude of a normalized vector is 1
     */
    public function testScenario17(): void
    {
        $v = Tuple::vector(1.0, 2.0, 3.0);
        $norm = $v->normalize();

        self::assertSameFloat(1.0, $norm->magnitude());
    }

    /**
     * @testdox The dot product of two tuples
     */
    public function testScenario18(): void
    {
        $a = Tuple::vector(1.0, 2.0, 3.0);
        $b = Tuple::vector(2.0, 3.0, 4.0);

        $dot = Tuple::dot($a, $b);

        self::assertSameFloat(20.0, $dot);
    }

    /**
     * @testdox The cross product of two vectors
     */
    public function testScenario19(): void
    {
        $a = Tuple::vector(1.0, 2.0, 3.0);
        $b = Tuple::vector(2.0, 3.0, 4.0);

        self::assertSameTuple(Tuple::vector(-1.0, 2.0, -1.0), Tuple::cross($a, $b));
        self::assertSameTuple(Tuple::vector(1.0, -2.0, 1.0), Tuple::cross($b, $a));
    }

    /**
     * @testdox Colors are (red, green, blue) tuples
     */
    public function testScenario20(): void
    {
        $c = Tuple::color(-0.5, 0.4, 1.7);

        self::assertSameFloat(-0.5, $c->x());
        self::assertSameFloat(0.4, $c->y());
        self::assertSameFloat(1.7, $c->z());
    }

    /**
     * @testdox Adding colors
     */
    public function testScenario21(): void
    {
        $c1 = Tuple::color(0.9, 0.6, 0.75);
        $c2 = Tuple::color(0.7, 0.1, 0.25);

        $c3 = Tuple::add($c1, $c2);

        self::assertSameTuple(Tuple::color(1.6, 0.7, 1.0), $c3);
    }

    /**
     * @testdox Subtracting colors
     */
    public function testScenario22(): void
    {
        $c1 = Tuple::color(0.9, 0.6, 0.75);
        $c2 = Tuple::color(0.7, 0.1, 0.25);

        $c3 = Tuple::sub($c1, $c2);

        self::assertSameTuple(Tuple::color(0.2, 0.5, 0.5), $c3);
    }

    /**
     * @testdox Multiplying a color by a scalar
     */
    public function testScenario23(): void
    {
        $c = Tuple::color(0.2, 0.3, 0.4);

        self::assertSameTuple(Tuple::color(0.4, 0.6, 0.8), Tuple::mul(2.0, $c));
    }

    /**
     * @testdox Multiplying colors
     */
    public function testScenario24(): void
    {
        $c1 = Tuple::color(1.0, 0.2, 0.4);
        $c2 = Tuple::color(0.9, 1.0, 0.1);

        $c3 = Tuple::hadamard($c1, $c2);

        self::assertSameTuple(Tuple::color(0.9, 0.2, 0.04), $c3);
    }

    /**
     * @testdox Reflecting a vector approaching at 45°
     */
    public function testScenario25(): void
    {
        $v = Tuple::vector(1.0, -1.0, 0.0);
        $n = Tuple::vector(0.0, 1.0, 0.0);

        $r = $n->reflect($v);

        self::assertSameTuple(Tuple::vector(1.0, 1.0, 0.0), $r);
    }

    /**
     * @testdox Reflecting a vector off a slanted surface
     */
    public function testScenario26(): void
    {
        $v = Tuple::vector(0.0, -1.0, 0.0);
        $n = Tuple::vector(\sqrt(2.0) / 2.0, \sqrt(2.0) / 2.0, 0.0);

        $r = $n->reflect($v);

        self::assertSameTuple(Tuple::vector(1.0, 0.0, 0.0), $r);
    }
}
