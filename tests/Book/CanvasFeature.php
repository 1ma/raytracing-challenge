<?php

declare(strict_types=1);

namespace UMA\Raytracer\Tests\Book;

use UMA\Raytracer\Core\Canvas;
use UMA\Raytracer\Core\Tuple;
use UMA\Raytracer\Tests\BookTestCase;

/**
 * @testdox CanvasFeature, part of Chapter 2: Drawing on a Canvas
 */
final class CanvasFeature extends BookTestCase
{
    /**
     * This test is not part of the book
     *
     * @testdox This test only exists to avoid polluting the execution time read in testTwo
     */
    public function testOne(): void
    {
    }

    /**
     * This test is not part of the book
     *
     * @testdox This test measures the time taken by PHPUnit to run nothing
     */
    public function testTwo(): void
    {
    }

    /**
     * @testdox Creating a canvas
     */
    public function testScenario1(): void
    {
        $c = new Canvas(10, 20);

        self::assertSame(10, $c->width());
        self::assertSame(20, $c->height());

        for ($i = 0; $i < $c->width(); $i++) {
            for ($j = 0; $j < $c->height(); $j++) {
                self::assertSameTuple(Tuple::color(0.0, 0.0, 0.0), $c->getPixel($i, $j));
            }
        }
    }

    /**
     * @testdox Writing pixels to a canvas
     */
    public function testScenario2(): void
    {
        $c = new Canvas(10, 20);
        $red = Tuple::color(1.0, 0.0, 0.0);

        $c->setPixel(2, 3, $red);

        self::assertSameTuple(Tuple::color(1.0, 0.0, 0.0), $c->getPixel(2, 3));
    }

    /**
     * @testdox Constructing the PPM header
     */
    public function testScenario3(): void
    {
        $c = new Canvas(5, 3);

        self::assertStringStartsWith(<<<'PPM'
P3
5 3
255

PPM
            , $c->toPPM());
    }

    /**
     * @testdox Constructing the PPM pixel data
     */
    public function testScenario4(): void
    {
        $c = new Canvas(5, 3);

        $c->setPixel(0, 0, Tuple::color(1.5, 0.0, 0.0));
        $c->setPixel(2, 1, Tuple::color(0.0, 0.5, 0.0));
        $c->setPixel(4, 2, Tuple::color(-0.5, 0.0, 1.0));

        self::assertStringEndsWith(<<<'PPM'
255 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 128 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 255

PPM
, $c->toPPM());
    }

    /**
     * @testdox Splitting long lines in PPM files
     */
    public function testScenario5(): void
    {
        $c = new Canvas(10, 2);

        for ($i = 0; $i < 10; $i++) {
            for ($j = 0; $j < 2; $j++) {
                $c->setPixel($i, $j, Tuple::color(1.0, 0.8, 0.6));
            }
        }

        self::assertStringEndsWith(<<<'PPM'
255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204
153 255 204 153 255 204 153 255 204 153 255 204 153
255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204
153 255 204 153 255 204 153 255 204 153 255 204 153

PPM
            , $c->toPPM());
    }

    /**
     * @testdox PPM files are terminated by a newline character
     */
    public function testScenario6(): void
    {
        $c = new Canvas(10, 2);

        self::assertStringEndsWith("\n", $c->toPPM());
    }
}
