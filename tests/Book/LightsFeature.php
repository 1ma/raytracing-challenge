<?php

declare(strict_types=1);

namespace UMA\Raytracer\Tests\Book;

use UMA\Raytracer\Core\PointLight;
use UMA\Raytracer\Core\Tuple;
use UMA\Raytracer\Tests\BookTestCase;

/**
 * @testdox LightsFeature, Chapter 6: Light and Shading
 */
final class LightsFeature extends BookTestCase
{
    /**
     * @testdox A point light has a position and intensity
     */
    public function testScenario1(): void
    {
        $intensity = Tuple::color(1.0, 1.0, 1.0);
        $position = Tuple::point(0.0, 0.0, 0.0);

        $pointLight = new PointLight($intensity, $position);

        self::assertSame($intensity, $pointLight->intensity());
        self::assertSame($position, $pointLight->position());
    }
}
