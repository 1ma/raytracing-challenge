<?php

declare(strict_types=1);

namespace UMA\Raytracer\Tests\Book;

use UMA\Raytracer\Core\Intersection;
use UMA\Raytracer\Core\Intersections;
use UMA\Raytracer\Core\Ray;
use UMA\Raytracer\Core\Sphere;
use UMA\Raytracer\Core\Tuple;
use UMA\Raytracer\Tests\BookTestCase;

/**
 * @testdox IntersectionsFeature, part of Chapter 5: Ray-Sphere Intersections
 */
final class IntersectionsFeature extends BookTestCase
{
    /**
     * @testdox An intersection encapsulates t and object
     */
    public function testScenario1(): void
    {
        $s = new Sphere();
        $i = new Intersection(3.5, $s);

        self::assertSameFloat(3.5, $i->t());
        self::assertSame($s, $i->shape());
    }

    /**
     * @testdox Aggregating intersections
     */
    public function testScenario2(): void
    {
        $s = new Sphere();
        $i1 = new Intersection(1.0, $s);
        $i2 = new Intersection(2.0, $s);

        $xs = new Intersections($i1, $i2);

        self::assertSame(2, $xs->count());
        self::assertSameFloat(1.0, $xs->get(0)->t());
        self::assertSameFloat(2.0, $xs->get(1)->t());
    }

    /**
     * @testdox Intersect sets the object on the intersection
     */
    public function testScenario3(): void
    {
        $r = new Ray(Tuple::point(0.0, 0.0, -5.0), Tuple::vector(0.0, 0.0, 1.0));
        $s = new Sphere();

        $xs = $s->intersect($r);

        self::assertSame(2, $xs->count());
        self::assertSame($s, $xs->get(0)->shape());
        self::assertSame($s, $xs->get(1)->shape());
    }

    /**
     * @testdox The hit, when all intersections have positive t
     */
    public function testScenario4(): void
    {
        $s = new Sphere();
        $i1 = new Intersection(1.0, $s);
        $i2 = new Intersection(2.0, $s);

        $xs = new Intersections($i2, $i1);

        self::assertSame($i1, $xs->hit());
    }

    /**
     * @testdox The hit, when some intersections have negative t
     */
    public function testScenario5(): void
    {
        $s = new Sphere();
        $i1 = new Intersection(-1.0, $s);
        $i2 = new Intersection(1.0, $s);

        $xs = new Intersections($i2, $i1);

        self::assertSame($i2, $xs->hit());
    }

    /**
     * @testdox The hit, when all intersections have negative t
     */
    public function testScenario6(): void
    {
        $s = new Sphere();
        $i1 = new Intersection(-2.0, $s);
        $i2 = new Intersection(-1.0, $s);

        $xs = new Intersections($i2, $i1);

        self::assertNull($xs->hit());
    }

    /**
     * @testdox The hit is always the lowest nonnegative intersection
     */
    public function testScenario7(): void
    {
        $s = new Sphere();
        $i1 = new Intersection(5.0, $s);
        $i2 = new Intersection(7.0, $s);
        $i3 = new Intersection(-3.0, $s);
        $i4 = new Intersection(2.0, $s);

        $xs = new Intersections($i1, $i2, $i3, $i4);

        self::assertSame($i4, $xs->hit());
    }
}
