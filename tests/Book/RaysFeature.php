<?php

declare(strict_types=1);

namespace UMA\Raytracer\Tests\Book;

use UMA\Raytracer\Core\Matrix;
use UMA\Raytracer\Core\Ray;
use UMA\Raytracer\Core\Tuple;
use UMA\Raytracer\Tests\BookTestCase;

/**
 * @testdox RaysFeature, part of Chapter 5: Ray-Sphere Intersections
 */
final class RaysFeature extends BookTestCase
{
    /**
     * @testdox Creating and querying a ray
     */
    public function testScenario1(): void
    {
        $origin = Tuple::point(1.0, 2.0, 3.0);
        $direction = Tuple::vector(4.0, 5.0, 6.0);
        $r = new Ray($origin, $direction);

        self::assertSame($origin, $r->origin());
        self::assertSame($direction, $r->direction());
    }

    /**
     * @testdox Computing a point from a distance
     */
    public function testScenario2(): void
    {
        $r = new Ray(Tuple::point(2.0, 3.0, 4.0), Tuple::vector(1.0, 0.0, 0.0));

        self::assertSameTuple(Tuple::point(2.0, 3.0, 4.0), $r->position(0.0));
        self::assertSameTuple(Tuple::point(3.0, 3.0, 4.0), $r->position(1.0));
        self::assertSameTuple(Tuple::point(1.0, 3.0, 4.0), $r->position(-1.0));
        self::assertSameTuple(Tuple::point(4.5, 3.0, 4.0), $r->position(2.5));
    }

    /**
     * @testdox Translating a ray
     */
    public function testScenario3(): void
    {
        $r = new Ray(Tuple::point(1.0, 2.0, 3.0), Tuple::vector(0.0, 1.0, 0.0));
        $m = Matrix::translation(3.0, 4.0, 5.0);

        $r2 = $r->transform($m);

        self::assertSameTuple(Tuple::point(4.0, 6.0, 8.0), $r2->origin());
        self::assertSameTuple(Tuple::vector(0.0, 1.0, 0.0), $r2->direction());
    }

    /**
     * @testdox Scaling a ray
     */
    public function testScenario4(): void
    {
        $r = new Ray(Tuple::point(1.0, 2.0, 3.0), Tuple::vector(0.0, 1.0, 0.0));
        $m = Matrix::scaling(2.0, 3.0, 4.0);

        $r2 = $r->transform($m);

        self::assertSameTuple(Tuple::point(2.0, 6.0, 12.0), $r2->origin());
        self::assertSameTuple(Tuple::vector(0.0, 3.0, 0.0), $r2->direction());
    }
}
