<?php

declare(strict_types=1);

namespace UMA\Raytracer\Tests;

use PHPUnit\Framework\TestCase;
use UMA\Raytracer\Core\Floats;
use UMA\Raytracer\Core\Matrix;
use UMA\Raytracer\Core\Tuple;

/**
 * Common assertion methods for the TestCases
 */
abstract class BookTestCase extends TestCase
{
    public static function assertSameFloat(float $expected, float $actual): void
    {
        self::assertTrue(
            Floats::equivalent($expected, $actual),
            "$expected and $actual are not the same float"
        );
    }

    public static function assertSameTuple(Tuple $expected, Tuple $actual): void
    {
        $expectedStr = self::tuple2string($expected);
        $actualStr = self::tuple2string($actual);

        self::assertTrue(
            Tuple::equals($expected, $actual),
            "$expectedStr and $actualStr are not the same tuple"
        );
    }

    public static function assertSameMatrix(Matrix $expected, Matrix $actual): void
    {
        self::assertTrue(
            Matrix::equals($expected, $actual),
            \sprintf("Expected Matrix:\n%s\nBut got:\n%s", $expected, $actual)
        );
    }

    private static function tuple2string(Tuple $t): string
    {
        return "({$t->x()},{$t->y()},{$t->z()},{$t->w()})";
    }
}
